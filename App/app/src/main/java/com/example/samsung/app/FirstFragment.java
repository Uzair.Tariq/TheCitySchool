package com.example.samsung.app;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by SAMSUNG on 7/24/2016.
 */
public class FirstFragment extends Fragment implements View.OnClickListener {

    View myView;

    ImageView bgimageid;
    TextView text01, weburl;
    Animation animBlink;

    static public Handler handler = new Handler();

    int array[] = {R.drawable.bga, R.drawable.bgb, R.drawable.bgc, R.drawable.bgd};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.first_layout, container, false);
        bgimageid = (ImageView) myView.findViewById(R.id.bgimageid);
        weburl = (TextView) myView.findViewById(R.id.weburl);
        text01 = (TextView) myView.findViewById(R.id.text01);

        weburl.setOnClickListener(this);

        SetBackgroundImage();
        mission01();
        return myView;
    }

    public void SetBackgroundImage() {

        Random r = new Random();
        int i1 = r.nextInt(4 - 0) + 0;

        bgimageid.setImageResource(array[i1]);

    }

    public void mission01() {
        text01.setText("OUR PHILOSOPHY\r\nContinuous Human Resource Development");
        // load the animation
        animBlink = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
                R.anim.blink);
        // start the animation
        text01.startAnimation(animBlink);

        final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
       mission02();
                }
           }, 4000);

    }

    public void mission02() {
        text01.setText("OUR GOAL\r\nAcademic Excellence");
        // load the animation
        animBlink = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
                R.anim.blink);
        // start the animation
        text01.startAnimation(animBlink);

        final Handler handler = new Handler();
          handler.postDelayed(new Runnable() {
              @Override
 public void run() {
                mission03();
            }
        }, 4000);
    }

    public void mission03() {
        text01.setText("OUR COMMITMENT\r\nTotal Satisfaction");
        // load the animation
        animBlink = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
                R.anim.blink);
        // start the animation
        text01.startAnimation(animBlink);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mission04();
            }
        }, 4000);
    }

    public void mission04() {
        text01.setText("OUR CULTURE\r\nProfessionalism");
        // load the animation
        animBlink = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
                R.anim.blink);
        // start the animation
        text01.startAnimation(animBlink);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mission01();
            }
        }, 4000);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.weburl:
                Intent httpIntent = new Intent(Intent.ACTION_VIEW);
                httpIntent.setData(Uri.parse("http://www.thecityschool.edu.pk"));

                startActivity(httpIntent);
                break;
        }

    }

}
